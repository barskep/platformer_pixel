﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;

[RequireComponent (typeof (Button))]
public class UnityAdsButton : MonoBehaviour {

    public string placementId = "rewardedVideo";
    public ItemBase database;
    private Button adButton;

#if UNITY_IOS
   private string gameId = "3256182";
#elif UNITY_ANDROID
    private string gameId = "3256183";
#endif

    void Start () {
        adButton = GetComponent<Button> ();
        if (adButton) {
            adButton.onClick.AddListener (ShowAd);
        }

        if (Monetization.isSupported) {
            Monetization.Initialize (gameId, true);
        }
    }

    void Update () {
        if (adButton) {
            adButton.interactable = Monetization.IsReady (placementId);
        }
    }

    void ShowAd () {
        ShowAdCallbacks options = new ShowAdCallbacks ();
        options.finishCallback = HandleShowResult;
        ShowAdPlacementContent ad = Monetization.GetPlacementContent (placementId) as ShowAdPlacementContent;
        ad.Show (options);
    }

    void HandleShowResult (ShowResult result) {
        if (result == ShowResult.Finished) {
            foreach (var item in database.Items)
            {
                GameManager.Instance.inventory.Items.Add(item);
            }

            if (GameManager.Instance.inventoryUiController != null)
            {
                GameManager.Instance.inventoryUiController.UpdateInventory();
            }
        } else if (result == ShowResult.Skipped) {
            Debug.LogWarning ("The player skipped the video - DO NOT REWARD!");
        } else if (result == ShowResult.Failed) {
            Debug.LogError ("Video failed to show");
        }
    }
}
